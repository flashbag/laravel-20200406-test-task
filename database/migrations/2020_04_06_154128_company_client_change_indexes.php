<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanyClientChangeIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_client', function (Blueprint $table){

            $table->dropIndex('company_client_company_id_index');
            $table->dropIndex('company_client_client_id_index');

            $table->unique(['company_id', 'client_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_client', function (Blueprint $table){

            $table->dropIndex('company_client_company_id_client_id_unique');

            $table->index('company_id');
            $table->index('client_id');
        });


    }
}
