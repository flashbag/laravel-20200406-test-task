<?php

use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $users = [
            [
                'name'  => 'admin',
                'email' => 'admin@site.com',
                'password' => Hash::make('admin'),
            ]
       ];

        foreach ($users as $user) {
            $user = User::updateOrCreate(['email' => $user['email']], $user);

            if (!$user->hasDefaultAPIToken()) {
                $user->createDefaultAPIToken();
            }
       }
    }
}
