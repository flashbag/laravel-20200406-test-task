<?php

use Illuminate\Database\Seeder;
use App\Models\{Company, Client};
use Illuminate\Support\Facades\DB;

class CompanyClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clientsIds = Client::select('id')->get()->pluck('id')->toArray();
        $companiesIds = Company::select('id')->get()->pluck('id')->toArray();


        $randomClientsCompanies = [];

        foreach ($clientsIds as $clientd) {
            // every second client
            if ($clientd % 2) {
                $randCompaniesCount = rand(1, 10);
                $randCompaniesIds = array_rand($companiesIds, $randCompaniesCount);

                if (!empty($randCompaniesIds)) {

                    if (is_array($randCompaniesIds)) {

                        foreach ($randCompaniesIds as $companyId) {

                            $randomClientsCompanies[] = [
                                'client_id'     => $clientd,
                                'company_id'    => $companyId
                            ];
                        }

                    } else {

                        $randomClientsCompanies[] = [
                            'client_id'     => $clientd,
                            'company_id'    => $randCompaniesIds
                        ];
                    }

                }
           }
        }

        DB::table('company_client')->truncate();
        DB::table('company_client')->insert($randomClientsCompanies);

    }
}
