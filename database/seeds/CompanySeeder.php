<?php

use Illuminate\Database\Seeder;

use App\Models\Company;

class CompanySeeder extends Seeder
{
    use ChunkGeneratingTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->chunkGenerating(10000, 500, Company::class, 'companies');
    }
}
