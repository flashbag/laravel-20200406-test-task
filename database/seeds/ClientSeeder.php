<?php

use Illuminate\Database\Seeder;

use App\Models\Client;
class ClientSeeder extends Seeder
{
    use ChunkGeneratingTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->chunkGenerating(10000, 1000, Client::class, 'clients');
    }
}
