<?php

trait ChunkGeneratingTrait
{
    public function chunkGenerating(int $limit, int $itemPerChunk, string $class, string $pluralName)
    {
        $countExisting = $class::count();
        $countToGenerate = $limit - $countExisting;

        if ($countToGenerate > 0) {
            $this->command->info('Has lower than ' . $limit . ' companies');

            $chunksCount = ceil($countToGenerate / $itemPerChunk);
            $this->command->info('Need to generate ' . $countToGenerate . ' companies');


            $generated = 0;
            for($i = 1; $i <= $chunksCount; $i++) {
                // last chunk

                $toGenerateCount = $itemPerChunk;

                if ($i == $chunksCount) {
                    $toGenerateCount = $countToGenerate - $generated;
                }

                factory($class, $toGenerateCount)->create();

                $generated += $toGenerateCount;

                $this->command->info('Generated ' . $toGenerateCount . ' '. $pluralName . '');

//                $mem = memory_get_usage();
//                var_dump($mem);

            }

            $this->command->info('All ' . $countToGenerate . ' '. $pluralName . ' generated!');


        } else {
            $this->command->info('Has ' . $limit . ' companies! Limit reached');
        }
    }
}
