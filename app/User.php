<?php

namespace App;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    const DEFAULT_TOKEN_NAME = 'default';

    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasDefaultAPIToken()
    {
        return $this->tokens()->where('name', self::DEFAULT_TOKEN_NAME)->count() > 0;
    }

    public function createDefaultAPIToken()
    {
        return $this->createToken(self::DEFAULT_TOKEN_NAME);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\MorphMany|null|object
     * @throws \Exception
     */
    public function getDefaultAPITokenAttribute()
    {
        if ($this->hasDefaultAPIToken()) {
            return $this->tokens()->where('name', self::DEFAULT_TOKEN_NAME)->first();
        }

        throw new \Exception(printf('user %i has no default API token!', $this->id));
    }
}
