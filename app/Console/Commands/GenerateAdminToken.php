<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Console\Command;

class GenerateAdminToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate admin token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::where('name', 'admin')->first();

        $token = $user->createToken('token-' . Str::random(32));

        dd($token->plainTextToken);
    }
}
