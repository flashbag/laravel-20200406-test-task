<?php

namespace App\Http\Controllers\API;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends APIController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->getList($request, Client::class, [ 'first_name', 'last_name' ], [], ['companies']);
    }

    public function getClientCompanies(Request $request, Client $client)
    {
        return response()->json($client->companies);
    }
}
