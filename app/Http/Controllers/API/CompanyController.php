<?php

namespace App\Http\Controllers\API;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends APIController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->getList($request, Company::class, [ 'name' ], [], ['clients']);
    }
}
