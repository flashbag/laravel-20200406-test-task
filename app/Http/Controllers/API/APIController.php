<?php
/**
 * Created by PhpStorm.
 * User: flashbag
 * Date: 06.04.20
 * Time: 17:43
 */

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class APIController extends Controller
{
    protected function getList(
        Request $request,
        string $className,
        array $searchable = [],
        array $with = [],
        array $withCount = []
    ) {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');

        $queryBuilder = $className::offset($start)->limit($length);

        $order = $request->get('order');

        if (!empty($order)) {
            $columnRaw = $order[0];
            $columnRequest = $request->get('columns')[$columnRaw['column']];

            $columnName = $columnRequest['data'];
            $columnDirection = $columnRaw['dir'];

            $queryBuilder->orderBy($columnName, $columnDirection);
        }

        if (!empty($request->get('search')['value']) && !empty($searchable)) {

            $searchQuery = [];

            $likeString = '%' . $request->get('search')['value'] . '%';

            foreach ($searchable as $field) {
                $searchQuery[] = [$field, 'like', $likeString];
            }

            $queryBuilder->where($searchQuery);
        }

        if (!empty($with)) {
            $queryBuilder->with($with);
        }

        if (!empty($withCount)) {
            $queryBuilder->withCount($withCount);
        }

//        dd($queryBuilder->toSql(), $queryBuilder->getBindings());

        $companies = $queryBuilder->get();

        $data = [
            'draw' => $draw,
            'recordsTotal' => $className::count(),
            'recordsFiltered' => $className::count(),
            'data' => $companies,
            'order' => $request->get('order')
        ];

        return response()->json($data);
    }
}
