<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Http\Requests\Admin\Client\{StoreClientRequest, UpdateClientRequest};
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        return view('admin.clients.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreClientRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClientRequest $request)
    {
        $validated = $request->validated();

        $client = Client::create($validated);

        return redirect(route('admin.clients.edit', ['client' => $client->id]))
            ->with('success', 'Client created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Client $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('admin.clients.edit', ['client' => $client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateClientRequest  $request
     * @param  Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClientRequest $request, Client $client)
    {
        $validated = $request->validated();

        $client->update($validated);

        return redirect(route('admin.clients.edit', ['client' => $client->id]))
            ->with('success', 'Client updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Client $client
     * @return void
     * @throws \Exception
     */
    public function destroy(Client $client)
    {
        $client->delete();

        session()->flash('success', 'Client "' . $client->first_name  . " " . $client->last_name . '" deleted successfully');
    }
}
