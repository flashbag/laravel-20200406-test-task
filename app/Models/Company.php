<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Company
 * @package App\Models
 * @property string $name
 */
class Company extends Model
{
    protected $fillable = [
        'name'
    ];

    /**
     * The roles that belong to the user.
     */
    public function clients()
    {
        return $this->belongsToMany(Client::class, 'company_client');
    }
}
