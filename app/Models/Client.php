<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Company
 * @package App\Models
 * @property string $first_name
 * @property string $last_name
 */
class Client extends Model
{
    protected $fillable = [
        'first_name',
        'last_name'
    ];

    /**
     * The roles that belong to the user.
     */
    public function companies()
    {
        return $this->belongsToMany(Company::class, 'company_client');
    }
}
