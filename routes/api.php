<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'as' => 'api.',
    'middleware' => 'auth:sanctum'
], function (){
    Route::apiResource('companies', 'API\CompanyController')->only(['index']);
    Route::apiResource('clients', 'API\ClientController')->only(['index']);

    Route::get('clients/{client}/companies', 'API\ClientController@getClientCompanies')
        ->name('api.clients.companies');
});
