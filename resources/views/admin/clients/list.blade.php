@extends('adminlte::page')

@section('title', 'Companies')

@section('content')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Clients</h3>
            <a href="{{ route('admin.clients.create') }}">Create client</a>
        </div>

        <br>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="clients" class="table table-bordered table-hover dataTable" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Companies count</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Companies count</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@stop

@section('js')
    <script>

        $(document).ready(function() {
            $('#clients').DataTable( {
                serverSide: true,
                ajax: {
                    url: "{{ route('api.clients.index') }}",
                    dataSrc: 'data'
                },
                columns: [
                    { data: 'id' },
                    { data: 'first_name' },
                    { data: 'last_name' },
                    { data: 'companies_count' },
                    {
                        sortable: false,
                        "render": function ( data, type, full, meta ) {
                            let href = '/clients/' + full.id + '/edit';
                            return '<a href="' + href + '"  >Edit</a>';
                        }
                    },
                    {
                        sortable: false,
                        "render": function ( data, type, full, meta ) {
                            let href = '/clients/' + full.id;
                            return '<a href="#" data-href="' + href + '" class="remove-item" >Delete</a>';
                        }
                    }
                ]
            });
        });

    </script>
@stop
