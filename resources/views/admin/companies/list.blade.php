@extends('adminlte::page')

@section('title', 'Companies')

@section('content')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Companies</h3>
            <a href="{{ route('admin.companies.create') }}">Create company</a>
        </div>

        <br>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="companies" class="table table-bordered table-hover dataTable" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Clients count</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Clients count</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@stop

@section('js')
    <script>

        $(document).ready(function() {
            $('#companies').DataTable( {
                serverSide: true,
                ajax: {
                    url: "{{ route('api.companies.index') }}",
                    dataSrc: 'data'
                },
                columns: [
                    { data: 'id' },
                    { data: 'name' },
                    { data: 'clients_count' },
                    {
                        sortable: false,
                        "render": function ( data, type, full, meta ) {
                            let href = '/companies/' + full.id + '/edit';
                            return '<a href="' + href + '"  >Edit</a>';
                        }
                    },
                    {
                        sortable: false,
                        "render": function ( data, type, full, meta ) {
                            let href = '/companies/' + full.id;
                            return '<a href="#" style="" data-href="' + href + '" class="remove-item" >Delete</a>';
                        }
                    }
                ]
            });
        });

    </script>
@stop
